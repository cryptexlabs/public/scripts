#!/usr/bin/env bash
set -e

realpath() {
    [[ $1 = /* ]] && echo "$1" || echo "$PWD/${1#./}"
}

cd "${PROJECT_DIRECTORY}"

createGitHook() {
    hook=$1
    curl https://gitlab.com/cryptexlabs/public/scripts/raw/master/template/git/hooks/${hook} -o "${PROJECT_DIRECTORY}/.git/hooks/${hook}"
    chmod +x "${PROJECT_DIRECTORY}/.git/hooks/${hook}"
}
echo "Installing git hooks"
createGitHook pre-commit
createGitHook post-merge

echo "Installing node_modules"

echo "Installing node_modules for ./app"
cd "${PROJECT_DIRECTORY}/app"
yarn install

if [[ -d "${PROJECT_DIRECTORY}/stubs" ]]; then
    for directory in ${PROJECT_DIRECTORY}/stubs/* ; do
        directory_name=$(basename "${directory}")
        echo "Installing node_modules for ./stubs/${directory_name}"
        cd "${directory}"
        yarn install
    done
fi

echo "Creating .env files from examples"
find "${PROJECT_DIRECTORY}" -name "*.example.env" -type f | while read example_path; do

    example_name=$(basename "${example_path}")
    example_directory=$(dirname "${example_path}")
    file_name=${example_name/.example/}
    file_path="${example_directory}/${file_name}"

    if [[ ! -f "${file_path}" ]]; then
        echo "creating ${file_path} from example file ${example_path}"
        cp "${example_path}" "${file_path}"
    else
        echo "${file_path} already exists"
    fi
done

echo "Creating docker-compose.override.yml"
if [[ ! -f "${PROJECT_DIRECTORY}/docker-compose.override.yml" ]]; then
    if [[ -f "${PROJECT_DIRECTORY}/docker-compose.override.example.yml" ]]; then
        cp "${PROJECT_DIRECTORY}/docker-compose.override.example.yml" "${PROJECT_DIRECTORY}/docker-compose.override.yml"
    else
        echo "No docker-compose.override.example.yml file exists in ${PROJECT_DIRECTORY} to create a docker-compose.override.yml from. Skipping."
    fi
else
    echo "docker-compose.override.yml already exists. Skipping."
fi